/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package pm.tap.sdk;

public final class R {
    public static final class anim {
        public static int left_in=0x7f040000;
        public static int left_out=0x7f040001;
        public static int right_in=0x7f040002;
        public static int right_out=0x7f040003;
    }
    public static final class attr {
    }
    public static final class color {
        public static int content_background=0x7f060001;
        public static int header_background=0x7f060000;
        public static int header_title_color=0x7f060002;
        /**  Inner page 
         */
        public static int inner_page_background=0x7f060003;
        public static int inner_page_carousel_background=0x7f060006;
        public static int inner_page_header_background=0x7f060004;
        public static int inner_page_title_background=0x7f060005;
    }
    public static final class drawable {
        public static int el=0x7f020000;
        public static int el1=0x7f020001;
        public static int header=0x7f020002;
        public static int header_block_background=0x7f020003;
        public static int notification_icon=0x7f020004;
        public static int open_arrow=0x7f020005;
        public static int play_button=0x7f020006;
        public static int play_icon=0x7f020007;
        public static int stars=0x7f020008;
        public static int stroke=0x7f020009;
        public static int top_header=0x7f02000a;
        public static int x_button=0x7f02000b;
    }
    public static final class id {
        public static int authButton=0x7f090028;
        public static int carousel_circles=0x7f090011;
        public static int carousel_lazy_img=0x7f090023;
        public static int carousel_lazy_progressbar=0x7f090024;
        public static int carousel_spacer=0x7f090010;
        public static int column=0x7f090025;
        public static int column_img=0x7f090026;
        public static int featured=0x7f09002b;
        public static int featured_big_img=0x7f090030;
        public static int featured_bottom_line=0x7f09002f;
        public static int featured_lines=0x7f09002c;
        public static int featured_space=0x7f09002e;
        public static int featured_top_line=0x7f09002d;
        public static int footer_space=0x7f090031;
        public static int game_page_back_button=0x7f090006;
        public static int game_page_container=0x7f090005;
        public static int game_page_frame=0x7f090004;
        public static int games_list=0x7f090022;
        public static int header_block=0x7f09001d;
        public static int header_play_icon=0x7f09001f;
        public static int header_space=0x7f090029;
        public static int header_title=0x7f09001e;
        public static int header_x_icon=0x7f090020;
        public static int inner_admob_block=0x7f090015;
        public static int inner_description_block=0x7f090012;
        public static int inner_footer_play_button=0x7f090017;
        public static int inner_game_description=0x7f090014;
        public static int inner_page=0x7f090007;
        public static int inner_page_carousel_container=0x7f09000e;
        public static int inner_page_game_name=0x7f09000b;
        public static int inner_page_game_rating=0x7f090013;
        public static int inner_page_header=0x7f090008;
        public static int inner_page_logo=0x7f090009;
        public static int inner_page_scroll=0x7f09000d;
        public static int inner_page_title_block=0x7f09000a;
        public static int inner_page_viewflipper=0x7f09000f;
        public static int inner_play_button=0x7f09000c;
        public static int inner_play_button_frame=0x7f090016;
        public static int inner_related_games=0x7f090019;
        public static int inner_related_games_title=0x7f090018;
        public static int loading_screen=0x7f090000;
        public static int loading_screen_animation=0x7f090002;
        public static int loading_screen_progressbar=0x7f090001;
        public static int loading_screen_text=0x7f090003;
        public static int row=0x7f09002a;
        public static int scroll_container=0x7f090021;
        public static int space=0x7f090032;
        public static int splash_screen=0x7f09001a;
        public static int splash_screen_img=0x7f09001c;
        public static int splash_screen_text=0x7f09001b;
        public static int stroke=0x7f090027;
    }
    public static final class layout {
        public static int activity_sdk=0x7f030000;
        public static int carousel_lazy_load=0x7f030001;
        public static int column=0x7f030002;
        public static int facebook_activity=0x7f030003;
        public static int row=0x7f030004;
        public static int space=0x7f030005;
    }
    public static final class raw {
        public static int imgpack=0x7f050000;
        public static int notification_sound=0x7f050001;
    }
    public static final class string {
        public static int app_name=0x7f070000;
        public static int play_now=0x7f070001;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static int AppTheme=0x7f080001;
    }
}
