package pm.tap.sdk;

import java.io.File;
import java.io.InputStream;

import pm.tap.sdk.libraries.Build;
import pm.tap.sdk.libraries.GameBySlug;
import pm.tap.sdk.libraries.SDKConfig;
import pm.tap.sdk.libraries.campaign.Campaign;
import pm.tap.sdk.libraries.interfaces.IEventDelegate;
import pm.tap.sdk.libraries.offerswall.OfferWallActivity;
import pm.tap.sdk.libraries.os.Preference;
import pm.tap.sdk.libraries.parse.Zip;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class Tap {
	
	//Tags
	private final static String TAG = "TapSDK";
	
	//Context
	private Activity ctx;
	
	//SDK Classes
	private Preference preference;
	private static Campaign campaign;
	
	//Interfaces
    private static IEventDelegate listener;
	
    //UI Properties
    //private String headerTitle;
    private String headerBackground;
    private String bodyBackground;
    private String splashScreenBackground;
    
    //SDK Properties
    private String appID;
    
    //Publisher
    private String publicKey;
    private String appKey;
    
    //Finals
    private static final String DEFAULT_SPLASH_TITLE = "More games";
    private static final String DEFAULT_HEADER_TITLE = "Play games";
    
    public Tap(Activity ctx, String appID)
    {
    	//Context 
    	this.ctx = ctx;
    	
    	//SDK Properties
    	this.appID = appID;
    	
    	//SDK Classes
    	this.preference = new Preference(ctx);

    	//Split unit id to Public key & Application key, Throws exception if failed.
    	try{
    		splitUnitID(appID);
    		
    		//Store the AppID
    		this.preference.putString(SDKConfig.APP_STORE_KEY, appID);
    		this.preference.commit();
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		Log.e(TAG, "Tap->construct, error: " + e.getMessage() + " - AppID: " + appID + ", PublicKey: " + publicKey + ", AppKey: " + appKey);
    	}
    	
    	//Create campaign 
    	campaign = new Campaign(this.ctx, this.publicKey, this.appKey, false);
    	campaign.startCampaign();
    	
    	//Send campaign event
    	try{
            campaign.onMoreGamesButtonShown();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    	
    	//Extract(Zip archive) the assets if needed
    	new AsyncTask<Void, Void, Void>(){

			@Override
			protected Void doInBackground(Void... params) {
				unzipIfMissing();
				return null;
			}
    		
    	}.execute();
    }
    
    public void setNotificationEnable(boolean enable)
    {
    	AlarmManager manager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
		
		try{
			preference.putBoolean(AlarmReceiver.ENABLE_NOTIFICATIONS, enable);
			preference.commit();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		    	
    	Intent alarmIntent = new Intent(ctx, AlarmReceiver.class);
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx, 0, alarmIntent, 0);
    	
    	if(!enable)
    	{
    		try{
    			manager.cancel(pendingIntent);
    		}
    		catch(Exception e){
    			e.printStackTrace();
    		}
    		
    		return; 
    	}
 
        int interval = (3600000 * 48); //48 Hours

        manager.setInexactRepeating(
        		AlarmManager.RTC_WAKEUP, 
        		System.currentTimeMillis(), 
        		interval, 
        		pendingIntent
        );
    }
    
    private void unzipIfMissing()
    {
    	SDKConfig.cachePath = Environment.getExternalStorageDirectory().toString() + '/' + this.ctx.getPackageName() + "/";
    	File cacheDir       = new File(SDKConfig.cachePath);
    	
        InputStream is = null;

        if(!cacheDir.exists())
            is = ctx.getResources().openRawResource(R.raw.imgpack);
        else
        {
            File[] contents = cacheDir.listFiles();

            if(contents == null || contents.length <= 0)
                is = ctx.getResources().openRawResource(R.raw.imgpack);
        }

        if(is != null)
            Zip.unzip(is, SDKConfig.cachePath);
    }
    
    /**
     * Select the background colors.
     * @param headerBackground Header background color(RGB)
     * @param bodyBackground Body background color(RGB)
     * @param splashBackground Splash screen background color(RGB)
     */
    public void setProperties(String headerBackground, String bodyBackground, String splashBackground)
    {
    	this.headerBackground       = headerBackground;
    	this.bodyBackground         = bodyBackground;
    	this.splashScreenBackground = splashBackground;
    }
        
    /**
     * Pop up "More games" window in game finish.
     * @param listener IEventDelegate interface
     */
    public void EndGameWithViewControllerAndAppID(IEventDelegate listener)
    {
    	showMoreGamesWithViewControllerAndAppID(listener);
    }
    
    /**
     * Pop up "More games" window.
     * @param listener IEventDelegate interface
     */
    public void showMoreGamesWithViewControllerAndAppID(IEventDelegate listener)
    {
        showMoreGamesWithViewControllerAndAppID(listener, DEFAULT_SPLASH_TITLE, null, false);
    }
    
   /**
    * Pop up "More games" window.
    * @param listener IEventDelegate interface
    * @param headerTitle Header title
    */
    public void showMoreGamesWithViewControllerAndAppID(IEventDelegate listener, String headerTitle)
    {
        showMoreGamesWithViewControllerAndAppID(listener, DEFAULT_SPLASH_TITLE, headerTitle, false);
    }
    
    /**
     * Pop up "More games" window.
     * @param listener IEventDelegate interface
     * @param Splash screen title
     * @param headerTitle Header title
     */
     public void showMoreGamesWithViewControllerAndAppID(IEventDelegate listener, String splashTitle, String headerTitle)
     {
         showMoreGamesWithViewControllerAndAppID(listener, splashTitle, headerTitle, false);
     }
     
     /**
      * Pop up interstitial window
      * @param listener IEventDelegate interface
      */
     public void InterstitialWithViewControllerAndAppID(IEventDelegate listener)
     {
     	showMoreGamesWithViewControllerAndAppID(listener, DEFAULT_SPLASH_TITLE, DEFAULT_HEADER_TITLE, true);
     }
    
    /**
     * Pop up interstitial window
     * @param listener IEventDelegate listener
     * @param splashText splashText Splash screen title
     * @param headerTitle Header title
     */
    public void InterstitialWithViewControllerAndAppID(IEventDelegate listener, String splashText, String headerTitle)
    {
    	showMoreGamesWithViewControllerAndAppID(listener, splashText, headerTitle, true);
    }
    
    /**
     * Show more games window
     * @param eventDelegate IEventDelegate interface
     * @param splashText Splash screen title
     * @param headerTitle Header title
     */
    public void showMoreGamesWithViewControllerAndAppID(IEventDelegate eventDelegate, String splashText, String headerTitle, boolean interstitial)
    {
        listener = eventDelegate;

        Intent tapPmSDK = new Intent(ctx, TapSDKActivity.class);

        tapPmSDK.putExtra(TapSDKActivity.APP_ID, appID);
        tapPmSDK.putExtra(TapSDKActivity.INTERSTITIAL, interstitial);

        tapPmSDK.putExtra(TapSDKActivity.HEADER_BACKGROUND, headerBackground);
        tapPmSDK.putExtra(TapSDKActivity.BODY_BACKGROUND, bodyBackground);
        tapPmSDK.putExtra(TapSDKActivity.SPLASH_SCREEN_BACKGROUND, splashScreenBackground);

        tapPmSDK.putExtra(TapSDKActivity.SPLASH_TEXT, splashText);
        tapPmSDK.putExtra(TapSDKActivity.HEADER_TITLE, headerTitle);

        ctx.startActivity(tapPmSDK);
    }
    
    public void showGameBySlug(final IEventDelegate eventDelegate, final String gameSlug)
    {
    	//Set static listener
		OfferWallActivity.listener = eventDelegate;
		OfferWallActivity.campaign = campaign;
		
    	GameBySlug.openGame(ctx, gameSlug);
    }
    
    public void showOffersWall(IEventDelegate eventDelegate)
    {
    	showOffersWall(eventDelegate, OfferWallActivity.DEFAULT_MONEY_NAME, OfferWallActivity.DEFAULT_MONEY_SCALE);
    }
    
    /**
     * Show offer wall window
     * @param eventDelegate IEventDelegate interface
     * @param moneyName Points name
     * @param scoreScale Score scale
     */
    public void showOffersWall(IEventDelegate eventDelegate, String moneyName, float scoreScale)
    {
    	try{
    		//Set static listener
    		OfferWallActivity.listener = eventDelegate;
    		OfferWallActivity.campaign = campaign;
        	
    		Intent offerWall = new Intent(ctx, OfferWallActivity.class);
    		offerWall.putExtra(OfferWallActivity.MONEY_NAME, moneyName);
    		offerWall.putExtra(OfferWallActivity.SCORE_SCALE, scoreScale);
    		
    		ctx.startActivity(offerWall);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }

    /**
     * Split UNIT_ID to public key & application key
     * @param appID Developer unit id
     */
    private void splitUnitID(String appID)
    {
    	String[] split = Build.splitAppID(appID);
    	
        if(split.length != 2)
            throw new RuntimeException(pm.tap.sdk.libraries.Build.INVALID_APP_ID); //Throw exception
        else
        {
            publicKey = split[0];
            appKey    = split[1];
        }
    }
    
    public static void establish(final Context context)
    {
    	try{
    		Build.establish(context);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }

    public void onResume()
    {
    	try{
            campaign.resumeCampaign();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public void onPause()
    {
    	try{
            campaign.pauseCampaign();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onDestroy()
    {
        try{
            campaign.stopCampaign();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    //Static methods
    //Getters
    public static IEventDelegate getListener(){
        return listener;
    }

    public static Campaign getCampaign()
    {
        return campaign;
    }
}
