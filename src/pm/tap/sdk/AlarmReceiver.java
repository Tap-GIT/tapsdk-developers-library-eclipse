package pm.tap.sdk;

import pm.tap.sdk.libraries.os.Preference;
import pm.tap.sdk.libraries.ui.Notifications;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class AlarmReceiver extends BroadcastReceiver{
	
	//Finals
    private static final String LAST_RATE_US_REQUEST = "last_rate_us_notification";
    private static final String IS_THE_FIRST_TIME    = "is_the_first_update";
    public static final String ENABLE_NOTIFICATIONS  = "enable_sdk_notifications";
    private static final int NOTIFICATION_EACH_TIME   = (3600000 * 24); //24 hours
	
	@Override
    public void onReceive(Context ctx, Intent intent) {
		Preference preference = new Preference(ctx);
		        
		try{
			if(!preference.getBoolean(ENABLE_NOTIFICATIONS,  true))
				return; //This options is disabled
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
        try{
            boolean isTheFirstUpdate = preference.getBoolean(IS_THE_FIRST_TIME, true);

            if(!isTheFirstUpdate)
            {
            	long lastRequest = preference.getLong(LAST_RATE_US_REQUEST, 0);
            	
            	if(lastRequest <= 0)
                {
                    preference.putLong(LAST_RATE_US_REQUEST, System.currentTimeMillis());
                    preference.commit();
                }
                else
                {
                    if((System.currentTimeMillis() - lastRequest) >= NOTIFICATION_EACH_TIME)
                    {
                    	Notifications n = new Notifications(ctx);
                        n.pop(R.drawable.notification_icon);
                        
                        preference.putLong(LAST_RATE_US_REQUEST, System.currentTimeMillis());
                        preference.commit();
                    }
                }
            	
            	
            }
            else
            {
                preference.putBoolean(IS_THE_FIRST_TIME, false);
                preference.commit();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
	}
	
}
