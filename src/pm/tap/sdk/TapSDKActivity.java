package pm.tap.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import pm.tap.sdk.libraries.Build;
import pm.tap.sdk.libraries.Screen;
import pm.tap.sdk.libraries.campaign.Campaign;
import pm.tap.sdk.libraries.interfaces.IEventDelegate;
import pm.tap.sdk.R;

public class TapSDKActivity extends Activity{
	
	//Tap SDK classes
    private Build build;
    private IEventDelegate listener;
    private Campaign campaign;
    
    //TapSDK properties
    private String appID;
    private boolean interstitial = false;
    
    //UI Properties
    private String headerTitle;
    private String headerBackground;
    private String bodyBackground;
    private String splashBackground;

    private Bitmap splashLogo = null;
    private String splashText = null;
    private String splashTextColor = null;    
	
    //Elements
    private View headerText;
    private View headerPlayIcon;
    private View headerXIcon;
    
    //Finals
    public static final String EXCEPTION_PREFIX = "TapPM SDK Exception: ";
    public static final int MAX_HEADER_TITLE_LENGTH = 17; //Max 17 chars
    
	//Extra keys
    public static final String HEADER_TITLE             = "header_title";
    public static final String HEADER_BACKGROUND        = "header_bg";
    public static final String BODY_BACKGROUND          = "body_bg";
    public static final String SPLASH_SCREEN_BACKGROUND = "splash_bg";
    public static final String SPLASH_LOGO              = "splash_logo";
    public static final String SPLASH_TEXT              = "splash_text";
    public static final String SPLASH_TEXT_COLOR        = "splash_text_COLOR";
    public static final String APP_ID                   = "app_id";
    public static final String INTERSTITIAL             = "interstitial";
    
    //Default values
    public static final String DEFAULT_HEADER_BG = "#1a2b3e";
    public static final String DEFAULT_BODY_BG   = "#0fa2d1";
    public static final String DEFAULT_SPLASH_BG = "#0fa2d1";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        
        Build.fullScreen(this);
        setContentView(R.layout.activity_sdk);
        
        this.listener = Tap.getListener();
        this.campaign = Tap.getCampaign();
        
        try{
        	Bundle bundle = getIntent().getExtras();

            headerTitle = bundle.getString(HEADER_TITLE);
            splashText  = bundle.getString(SPLASH_TEXT);
            
            headerBackground = bundle.getString(HEADER_BACKGROUND);
            headerBackground = (headerBackground == null) ? DEFAULT_HEADER_BG : headerBackground;
            
            bodyBackground   = bundle.getString(BODY_BACKGROUND);
            bodyBackground   = (bodyBackground == null) ? DEFAULT_BODY_BG : bodyBackground;

            splashBackground = bundle.getString(SPLASH_SCREEN_BACKGROUND);
            splashBackground = (splashBackground == null) ? DEFAULT_SPLASH_BG : splashBackground;

            if(splashText == null)
                splashLogo = bundle.getParcelable(SPLASH_LOGO);
            else
                splashTextColor = bundle.getString(SPLASH_TEXT_COLOR);
            
            appID        = bundle.getString(APP_ID);
            interstitial = bundle.getBoolean(INTERSTITIAL);
            
        }
        catch(Exception e){
        	e.printStackTrace();
        	
        	if(listener != null)
        		listener.viewConnectFail(EXCEPTION_PREFIX + e.getMessage());
        }
        
        try{
            if(campaign != null)
            {
                if(interstitial)
                    campaign.interstitialShown();
                else
                    campaign.onMoreGamesButtonClicked();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        this.headerText     = findViewById(pm.tap.sdk.R.id.header_title);
        this.headerPlayIcon = findViewById(pm.tap.sdk.R.id.header_play_icon);
        this.headerXIcon    = findViewById(pm.tap.sdk.R.id.header_x_icon);
        
        try{
        	build = new Build(
                    this, 
                    appID,
                    interstitial,
                    this.listener,
                    this.campaign,
                    pm.tap.sdk.R.id.header_block,
                    pm.tap.sdk.R.id.scroll_container,
                    pm.tap.sdk.R.id.games_list,
                    pm.tap.sdk.R.id.splash_screen,
                    pm.tap.sdk.R.id.game_page_container,
                    pm.tap.sdk.R.id.game_page_frame,
                    R.id.game_page_back_button
            );
        	
        	//Store layouts id
            build.setlayoutsId(
                    pm.tap.sdk.R.layout.row,
                    pm.tap.sdk.R.layout.column,
                    pm.tap.sdk.R.layout.space,
                    pm.tap.sdk.R.layout.carousel_lazy_load
            );

            //Store row elements id
            build.setRowElementsId(
                    pm.tap.sdk.R.id.header_space,
                    pm.tap.sdk.R.id.row,
                    pm.tap.sdk.R.id.footer_space,
                    pm.tap.sdk.R.id.column_img);

            //Store featured row elements id
            build.setFeaturedElementsId(
                    pm.tap.sdk.R.id.featured,
                    pm.tap.sdk.R.id.featured_big_img,
                    pm.tap.sdk.R.id.featured_top_line,
                    pm.tap.sdk.R.id.featured_space,
                    pm.tap.sdk.R.id.featured_bottom_line,
                    pm.tap.sdk.R.id.featured_lines,
                    pm.tap.sdk.R.id.stroke);

            //Store loading screen elements id
            build.setLoadingScreenElementsId(
                    pm.tap.sdk.R.id.loading_screen,
                    pm.tap.sdk.R.id.loading_screen_progressbar,
                    pm.tap.sdk.R.id.loading_screen_text,
                    pm.tap.sdk.R.id.loading_screen_animation
            );

            //Store inner page elements id
            build.setInnerPageElementsId(
                    pm.tap.sdk.R.id.inner_page,
                    pm.tap.sdk.R.id.inner_page_header,
                    pm.tap.sdk.R.id.inner_page_logo,
                    pm.tap.sdk.R.id.inner_page_title_block,
                    pm.tap.sdk.R.id.inner_page_game_name,
                    pm.tap.sdk.R.id.inner_page_game_rating,
                    pm.tap.sdk.R.id.inner_page_scroll,
                    pm.tap.sdk.R.id.inner_page_carousel_container,
                    pm.tap.sdk.R.id.inner_page_viewflipper,
                    pm.tap.sdk.R.anim.left_in,
                    pm.tap.sdk.R.anim.left_out,
                    pm.tap.sdk.R.anim.right_in,
                    pm.tap.sdk.R.anim.right_out,
                    pm.tap.sdk.R.id.carousel_lazy_img,
                    pm.tap.sdk.R.id.carousel_lazy_progressbar,
                    pm.tap.sdk.R.id.carousel_spacer,
                    pm.tap.sdk.R.id.carousel_circles,
                    pm.tap.sdk.R.id.inner_description_block,
                    pm.tap.sdk.R.id.inner_game_description,
                    pm.tap.sdk.R.id.inner_admob_block,
                    pm.tap.sdk.R.id.inner_footer_play_button,
                    pm.tap.sdk.R.id.inner_play_button_frame,
                    pm.tap.sdk.R.id.inner_related_games,
                    pm.tap.sdk.R.id.inner_related_games_title,
                    pm.tap.sdk.R.id.inner_play_button
            );

            //Store "raw" images pack
            build.setImgPackId(R.raw.imgpack);
            //build.setRawJsonId(R.raw.json);

            //Select background colors
            build.setBackgroundColors(headerBackground, bodyBackground, splashBackground);

            //Enable splash screen
            build.setSplashScreen(
                    splashLogo,
                    splashText,
                    splashTextColor,
                    pm.tap.sdk.R.id.splash_screen_img,
                    pm.tap.sdk.R.id.splash_screen_text
            );

            TextView hTitle = (TextView) findViewById(pm.tap.sdk.R.id.header_title);

            if(headerTitle != null)
            {
            	if(headerTitle.length() > MAX_HEADER_TITLE_LENGTH)
            		headerTitle = headerTitle.substring(0, MAX_HEADER_TITLE_LENGTH);
            	
                try{
                    hTitle.setText(headerTitle);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

            try{
                hTitle.setTextSize(pixelsToSp(this, (Screen.height(this) * 0.043f)));
            }
            catch (Exception e){
                e.printStackTrace();
            }

            build.run();
            updateUI();
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        
        //UI Events
        headerXIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
	
    private void updateUI()
    {
        final int orientation = getResources().getConfiguration().orientation;

        final int screenHeight = Screen.height(this);
        final int screenWidth  = Screen.width(this);

        int iconsSize      = (orientation == Configuration.ORIENTATION_LANDSCAPE) ? (int)(screenHeight * 0.09) : (int)(screenHeight * 0.07);
        int iconsTopMargin = (orientation == Configuration.ORIENTATION_LANDSCAPE) ? (int)(screenHeight * 0.02) : (int)(screenHeight * 0.01);

        int playLeftMargin = (int)(iconsSize * 0.3);

        //Play button params
        FrameLayout.LayoutParams playParams = new FrameLayout.LayoutParams(iconsSize, iconsSize);

        playParams.setMargins(playLeftMargin, iconsTopMargin, 0, 0);
        headerPlayIcon.setLayoutParams(playParams);

        //Title params
        FrameLayout.LayoutParams titleParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        titleParams.setMargins((playLeftMargin + iconsSize + (int)(iconsSize * 0.1)), 0, 0, 0);

        headerText.setLayoutParams(titleParams);

        //X button params
        FrameLayout.LayoutParams xButtonParams = new FrameLayout.LayoutParams(iconsSize, iconsSize);

        int screenWidthIcon = (screenWidth - iconsSize);
        xButtonParams.setMargins((int)(screenWidthIcon - (screenWidthIcon * 0.05)), iconsTopMargin, 0, 0);

        headerXIcon.setLayoutParams(xButtonParams);
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        updateUI();
        
        if(build != null)
        	build.run();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        try{
        	if(build != null)
        		build.onResume();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    public void onPause() {

        try{
        	if(build != null)
        		build.onPause();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        try{
        	if(build != null)
        		build.onDestroy();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        try{
        	if(build != null)
        		build.backEvent();
        }
        catch(Exception e){
        	e.printStackTrace();
        	finish();
        }
    }
}
